import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import axios from "axios";
import Button from "../components/Button";
import Gap from "../components/Gap";
import { setUser } from "../store/actionCreators";

const Home = () => {
  const user = useSelector((state) => state.user);
  const dispatch = useDispatch();
  const [listData, setListData] = useState([]);

  const logout = async (values) => {
    dispatch(setUser(null));
    alert("Success logout");
  };

  const fetchData = async () => {
    try {
      const response = await axios.get(
        "https://hp-api.onrender.com/api/characters"
      );
      setListData(response.data.filter((item, index) => index < 100));
    } catch (e) {
      console.error("error", e);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div
      style={{
        flex: 1,
        backgroundColor: "white",
        paddingHorizontal: 20,
      }}>
      <Gap bottom={20} />
      <div
        style={{
          fontSize: 22,
          color: "black",
        }}>
        Welcome, {user?.email || "Guest"}
      </div>
      <Gap bottom={20} />
      <Button title="Logout" onClick={logout} />
      <div
        style={{
          flexDirection: "row",
          flexWrap: "wrap",
          display: "flex",
        }}>
        {listData.map((item, index) => (
          <div
            key={index}
            style={{
              color: "black",
              textDecoration: "none",
              backgroundColor: "#ccc",
              marginBottom: 5,
              borderRadius: 10,
              padding: 10,
              width: "15%",
              margin: "1%",
            }}>
            <img
              src={item.image}
              alt="cover"
              style={{ width: "100%", height: 200 }}
            />
            <div
              style={{
                fontSize: 18,
                marginTop: 5,
                fontWeight: "bold",
                color: "#000",
              }}>
              {item.name}
            </div>
            <div style={{ color: "#333" }}>
              {item.dateOfBirth === ""
                ? item.yearOfBirth === ""
                  ? "Unknown"
                  : item.yearOfBirth
                : item.dateOfBirth}
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default Home;
