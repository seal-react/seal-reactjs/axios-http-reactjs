import React, { useEffect, useState } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import { useDispatch } from "react-redux";
import axios from "axios";

import Input from "../components/Input";
import Button from "../components/Button";
import Gap from "../components/Gap";
import { setUser } from "../store/actionCreators";

const FormSchema = Yup.object().shape({
  email: Yup.string()
    .email("Email format is invalid")
    .required("Email is required"),
  password: Yup.string().required("Password is required"),
});

const Login = ({ navigation }) => {
  const dispatch = useDispatch();
  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    validationSchema: FormSchema,
    onSubmit: (values) => submit(values),
  });

  const submit = async (values) => {
    try {
      const response = await axios.post(
        "https://8340fa0f-4afd-43b9-8593-2a23fcfa580c.mock.pstmn.io/login",
        formik.values
      );
      const { success, message } = response.data;

      if (success) {
        dispatch(setUser(values));
        alert(message);
      } else {
        alert(message);
      }
    } catch (e) {
      alert(e?.response?.data?.message || "Failed login");
      console.error("error", e);
    }
  };

  return (
    <div>
      <Input
        placeholder="name@email.com"
        value={formik.values.email}
        onChange={(e) => formik.setFieldValue("email", e.target.value)}
      />
      <Input
        type="password"
        placeholder="Input password here"
        value={formik.values.password}
        onChange={(e) => formik.setFieldValue("password", e.target.value)}
      />
      <Gap bottom={20} />
      <ul style={{ fontSize: 12 }}>
        {Object.keys(formik.errors).map((item, index) => (
          <li key={index}>{formik.errors[item]}</li>
        ))}
      </ul>
      <Button
        isDisabled={!formik.isValid}
        title="Login"
        onClick={formik.handleSubmit}
      />
    </div>
  );
};

export default Login;
